$(document).ready(function(){
	var date = new Date();
	var month = date.getMonth();
	var $sliderBy = $('#event-by-month');
	$sliderBy.slick({
		vertical: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		infinite: true,
		centerMode: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					vertical: false,
					slidesToShow: 3,
					slidesToScroll: 1
				}
			}
		]
	});
	
	$('.section-event .left-content .slick-slide').click(function(){
		$('.event-slider.active').removeClass('active');
		var $month = "." + $(this).attr('data-month');
		var $position = $(this).attr('data-slick-index');
		$sliderBy.slick('slickGoTo', $position);
		$($month + '.event-slider').addClass('active');
	});
	
	$('.event-slider.t' + (month + 1)).addClass('active');
	
	$('.count-event .slider-nav .next').click(function(){
		var $slick = $(this).parent().attr('data-slider');
		$(document).find($slick + ' .slick-next.slick-arrow').click();
	});
	$('.count-event .slider-nav .prev').click(function(){
		var $slick = $(this).parent().attr('data-slider');
		$(document).find($slick + ' .slick-prev.slick-arrow').click();
	});
		
	$('.owl-filter-bar .item').click(function(){
		if(!$(this).hasClass('active')){
			$('.owl-filter-bar .item.active').removeClass('active');
			$(this).addClass('active');
			var $elementFt = $(this).attr('data-owl-filter');
			$('.js-quan-11-travel-slider.active').removeClass('active');
			$($elementFt).addClass('active');
		}
	});
	
	/* Dulich Scroll Section */
	
	new fullpage('#scroll-parent', {
		loopHorizontal: true,
		scrollHorizontally: true,
		scrollingSpeed: 1000,
		responsiveWidth: 1199,
		scrollOverflow: true,
		afterRender: function(){
			$('.section-home-content.active .animate__animated').each(function(){
				var animateClass = $(this).data('animate');
				$(this).addClass(animateClass);
			});
			$('.section-home-content:not(.active) .animate__animated').each(function(){
				var animateClass = $(this).data('animate');
				$(this).removeClass(animateClass);
			});
			$(".menu-content-scroll-v2 ul li a").on("click",function(e) {
				e.preventDefault();
				var clickPos = $(this).parent().index() + 1;
				fullpage_api.silentMoveTo(clickPos);
			});
			$(".dulich-khampha-index .vertical-header .header-social .move_down").on("click",function() {
				fullpage_api.moveSectionDown();
			});
		},
		onLeave: function(origin, destination, direction){
			var $scrollToIndex = destination.index;
			var $current = $('#scroll-parent section').eq($scrollToIndex);
			var ref = $current.attr("data-section-name");
			$(".menu-content-scroll-v2 li.active").removeClass("active");
			$(".menu-content-scroll-v2 li.js-scroll-" + ref).addClass("active");
			$('.section-home-content').not(':eq('+$scrollToIndex+')').removeClass('show-element');
			$('.section-home-content').eq($scrollToIndex).addClass('show-element');
			$('.section-home-content').not(':eq('+$scrollToIndex+')').find('.animate__animated').each(function(){
				var animateClass = $(this).data('animate');
				$(this).removeClass(animateClass);
			});
			$('.section-home-content').eq($scrollToIndex).find('.animate__animated').each(function(){
				$(this).addClass('show-element');
				var animateClass = $(this).data('animate');
				$(this).addClass(animateClass);
			});
			if(ref === "su-kien") {
				$sliderBy.slick('slickGoTo', month);
			}
		}
	});
});