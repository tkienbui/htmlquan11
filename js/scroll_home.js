$(document).ready(function(){
	new fullpage('#scroll-parent', {
		loopHorizontal: true,
		scrollHorizontally: true,
		slidesNavigation: true,
		scrollingSpeed: 1000,
		responsiveWidth: 1199,
		scrollOverflow: true,
		scrollOverflowReset: true,
		afterRender: function(){
			$('.menu-content-scroll ul').addClass('delay-effect');
			$('.section-home-content.active .animate__animated').each(function(){
				var animateClass = $(this).data('animate');
				$(this).addClass(animateClass);
			});
			$('.section-home-content:not(.active) .animate__animated').each(function(){
				var animateClass = $(this).data('animate');
				$(this).removeClass(animateClass);
			});
			$(".menu-content-scroll ul li a").on("click",function(e) {
				e.preventDefault();
				var clickPos = $(this).parent().index() + 1;
				fullpage_api.silentMoveTo(clickPos);
			});
			timeOutMenuDelay = setTimeout(function(){
				$('.menu-content-scroll ul').removeClass('delay-effect');
			}, 3000);
		},
		onLeave: function(origin, destination, direction){
			clearTimeout(timeOutMenuDelay);
			var $scrollToIndex = destination.index;
			var $current = $('#scroll-parent section').eq($scrollToIndex);
			var ref = $current.attr("data-section-name");
			$(".menu-content-scroll li.active").removeClass("active");
			$(".menu-content-scroll li.js-scroll-" + ref).addClass("active");
			$('.section-home-content').not(':eq('+$scrollToIndex+')').find('.animate__animated').each(function(){
				var animateClass = $(this).data('animate');
				$(this).removeClass(animateClass);
			});
			$('.section-home-content').eq($scrollToIndex).find('.animate__animated').each(function(){
				var animateClass = $(this).data('animate');
				$(this).addClass(animateClass);
			});
			$('.menu-content-scroll ul').addClass('delay-effect');
			timeOutMenuDelay = setTimeout(function(){
				$('.menu-content-scroll ul').removeClass('delay-effect');
			}, 1200);
			if(ref === "introduction"){
				$('.vertical-header').addClass('ver-blue');
				$('.vertical-header').removeClass('ver-gray');
			}else if(ref === "location"){
				$('.vertical-header').addClass('ver-gray');
				$('.vertical-header').removeClass('ver-blue');
			}else {
				$('.vertical-header').removeClass('ver-gray');
				$('.vertical-header').removeClass('ver-blue');
			}
		}
	});
	$(".section-welcome .action-down").on("click",function() {
		fullpage_api.moveSectionDown();
	});
});